package com.handoko.neo4j.controller;

import com.handoko.neo4j.node.User;
import com.handoko.neo4j.repository.UserRepository;
import com.handoko.neo4j.request.CreateUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/create")
    public User createUser(@RequestBody CreateUserRequest createUserRequest) {
        List<User> users = new ArrayList<>();
        users.add(new User(UUID.randomUUID().toString(), createUserRequest.userTwo));

        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setName(createUserRequest.userOne);
        user.setFriends(users);

        userRepository.save(user);

        return user;
    }

    @PostMapping("/relation/create")
    public User createRelation(@RequestBody CreateUserRequest createUserRequest) {
        List<User> users = new ArrayList<>();
        users.add(new User(createUserRequest.idOne, createUserRequest.userOne));

        User user = userRepository.findByName(createUserRequest.userTwo);
        user.setFriends(users);

        userRepository.save(user);

        return user;
    }

    @GetMapping("/list")
    public List<User> getUsers() {
        return (List<User>) userRepository.findAll();
    }

    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") String id) {
        userRepository.deleteById(id);

        return "Successfully delete user";
    }
}
