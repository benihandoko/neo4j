package com.handoko.neo4j.controller;

import com.handoko.neo4j.node.Person;
import com.handoko.neo4j.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/person/create")
    public Person createPerson() {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person(UUID.randomUUID().toString(), "Adem"));

        Person person = new Person();
        person.setId(UUID.randomUUID().toString());
        person.setName("Handoko");
        person.setFollowers(persons);

        personRepository.save(person);

        return person;
    }

    @GetMapping("/person/update")
    public String updatePerson() {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person(UUID.randomUUID().toString(), "Acep"));

        Person person1 = personRepository.findByName("Asep");
        person1.setFollowers(persons);

        Person person2 = personRepository.findByName("Tatang");
        person2.setFollowers(persons);

        personRepository.save(person1);
        personRepository.save(person2);

        return "success";
    }

    @GetMapping("/person/list")
    public List<Person> getAllAudience() {
        return (List<Person>) personRepository.findAll();
    }

//    MATCH (N:Person{name:"Asep"}),(M:Person{name:"Tatang"})
//    WITH [(N)--(B)--(M) WHERE B:Person|B.name] AS NAMES
//    UNWIND NAMES AS MUTUAL_FRIEND
//    RETURN MUTUAL_FRIEND
}
