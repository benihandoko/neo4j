package com.handoko.neo4j.node;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@NodeEntity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    @Id
    private String id;
    private String name;

    @Relationship(type = "Friend", direction = Relationship.OUTGOING)
    private List<Person> followers;

    public Person(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
