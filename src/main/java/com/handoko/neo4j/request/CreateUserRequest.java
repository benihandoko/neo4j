package com.handoko.neo4j.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserRequest {

    public String idOne;
    public String idTwo;
    public String userOne;
    public String userTwo;
}
