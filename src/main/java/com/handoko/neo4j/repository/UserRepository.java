package com.handoko.neo4j.repository;

import com.handoko.neo4j.node.User;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface UserRepository extends Neo4jRepository<User, String> {
    User findByName(String userTwo);
}
