package com.handoko.neo4j.repository;

import com.handoko.neo4j.node.Person;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface PersonRepository extends Neo4jRepository<Person, String> {
    Person findByName(String name);
}
